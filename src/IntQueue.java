/**
 * int型Queueの自作
 */
public class IntQueue{
	int data[];	// データ保存用の配列
	int top;	// 格納データの先頭
	int count;	// データの個数
	private static int LimitSize = 100;

	// 例外
	public class EmptyIntQueueException extends RuntimeException{
		public EmptyIntQueueException(){
		}
	}
	// 例外
	public class OverflowIntQueueException extends RuntimeException{
		public OverflowIntQueueException(){}
	}
	// コンストラクタ
	IntQueue(){
		this(LimitSize);
	}
	IntQueue(int size){
		data = new int[size];
		top = 0;
		count = 0;
	}

	/**
	 * 可能な場合、指定された要素をこのキューに挿入します。
	 * @param x - 追加する要素
	 * @return このキューに要素が追加された場合は true
	 * @throws OverflowIntQueueException - キューがあふれた場合
	 */
	public boolean enqueue(int x){
		// 記憶領域があふれた場合は失敗
		if(count >= data.length){
			return false;
		}
		// TODO ここに作成（循環配列であることに注意）
		return true;
	}

	/**
	 * キューの先頭を取得および削除します
	 * @return　キューの先頭
	 * @throws EmptyIntQueueException　- キューが空の場合
	 */
	public int dequeue() throws EmptyIntQueueException{
		if(isEmpty()){
			throw new EmptyIntQueueException();		// 空の場合
		}
		// TODO ここに作成（循環配列であることに注意）
		int x=0;
		return x;
	}
	
	/**
	 * 削除せずにキューの先頭を取得する
	 * @return　キューの先頭 
	 * @throws EmptyIntQueueException - キューが空の場合
	 */
	public int peek() throws EmptyIntQueueException {
		if(isEmpty()){
			throw new EmptyIntQueueException();		// 空の場合
		}
		// TODO ここに作成（適切な値を返す）
		return 0;
	}
	/**
	 * キューに格納されている要素数を返す
	 * @return 要素数
	 */
	public int size() {
		return count;
	}

	/**
	 * キューが空かどうかを判断します
	 * @return 空の場合 true,そうでない場合 falseを返す
	 */
	public boolean isEmpty() {
		if(count==0)
			return true;
		else
			return false;
	}
	/* キューの内容をダンプする
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		String buf = "[ ";
		for (int i=0 ; i<count;i++)
			buf += data[(top+i)%data.length]+" ";
		return buf+"]";
	}
}
