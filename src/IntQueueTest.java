import static org.junit.Assert.*;
import org.junit.Test;

public class IntQueueTest {
	/*
	 * 'IntQueue.add(int)' のためのテスト・メソッド
	 */
	@Test
	public void Add() {
		IntQueue queue = new IntQueue(3);
		assertEquals(true,queue.enqueue(1));
		assertEquals(1,queue.count);
		assertEquals(0,queue.top);
		assertEquals(true,queue.enqueue(2));
		assertEquals(2,queue.count);
		assertEquals(0,queue.top);
		assertEquals(true,queue.enqueue(3));
		assertEquals(3,queue.count);
		assertEquals(0,queue.top);
		assertEquals(1,queue.data[0]); 
		assertEquals(2,queue.data[1]); 
		assertEquals(3,queue.data[2]); 
		assertEquals(false,queue.enqueue(4));	
		assertEquals(1,queue.dequeue());
		assertEquals(2,queue.count);
		assertEquals(1,queue.top);
		assertEquals(2,queue.dequeue());
		assertEquals(1,queue.count);
		assertEquals(2,queue.top);
		assertEquals(true,queue.enqueue(5));
		assertEquals(2,queue.count);
		assertEquals(2,queue.top);
		
	}
	/*
	 * 'IntQueue.poll()' のためのテスト・メソッド
	 */
	@Test
	public void Poll() {
		IntQueue queue = new IntQueue(3);
		queue.enqueue(1);
		queue.enqueue(2);
		assertEquals(1,queue.dequeue());
		assertEquals(2,queue.dequeue());	
		queue.enqueue(3);
		assertEquals(3,queue.dequeue());	
	}

	/*
	 * 'IntQueue.size()' のためのテスト・メソッド
	 */
	@Test
	public void Size() {
		IntQueue queue = new IntQueue(3);
		queue.enqueue(1);
		queue.enqueue(2);
		assertEquals(2,queue.size());
	}

	/*
	 * 'MyQueue.isEmpty()' のためのテスト・メソッド
	 */
	@Test
	public void isEmpty() {
		IntQueue queue = new IntQueue(3);
		assertEquals(true,queue.isEmpty());
		queue.enqueue(1);
		assertEquals(false,queue.isEmpty());
	}

}
