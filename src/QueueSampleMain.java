import java.util.concurrent.*;

public class QueueSampleMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		 IntQueue queue = new IntQueue(100);
		
		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);
		queue.enqueue(5);
		
		System.out.println("Queue is empty:"+queue.isEmpty());
		
		System.out.println("Poll of Stack:"+queue.peek());
		System.out.println("dequeue:"+queue.dequeue()+" "+queue.toString());
		System.out.println("dequeue:"+queue.dequeue()+" "+queue.toString());
		System.out.println("dequeue:"+queue.dequeue()+" "+queue.toString());

	}

}
